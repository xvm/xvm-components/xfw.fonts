"""
SPDX-License-Identifier: MIT
Copyright (c) 2018-2022 XVM Contributors
"""

#
# Imports
#

# stdlib
import logging
import os

#xfw.loader
import xfw_loader.python as loader
import openwg_vfs as vfs

# xfw.native
from xfw_native.python import XFWNativeModuleWrapper


#
# Globals
#

__native = None
__xfw_fonts_initialized = False

package_id = 'com.modxvm.xfw.fonts'



#
# Helpers
#

def __fix_path_slashes(path):
    """
    Replaces backslashes with slashes
    """

    if path:
        path = path.replace('\\', '/')
        if path[-1] != '/':
            path += '/'

    return path



def __resolve_path(path, basepath=None):
    """
    Resolves path to file

    'xvm://*' --> '../res_mods/mods/shared_resources/xvm/*'
    'res://*' --> '../res_mods/mods/shared_resources/*'
    'cfg://*' --> '../res_mods/configs/xvm/*'
    '*'       --> 'basepath/*'
    """

    if path[:6].lower() == "res://":
        path = path.replace("res://", loader.XFWLOADER_PATH_TO_ROOT + "res_mods/mods/shared_resources/", 1)
    elif path[:6].lower() == "xvm://":
        path = path.replace("xvm://", loader.XFWLOADER_PATH_TO_ROOT + "res_mods/mods/shared_resources/xvm/", 1)
    elif path[:6].lower() == "cfg://":
        path = path.replace("cfg://", loader.XFWLOADER_PATH_TO_ROOT + "res_mods/configs/xvm/", 1)
    elif basepath:
        path = __fix_path_slashes(basepath) + path

    return path.replace('\\', '/')


def __get_file(path):
    """
    Get absolute path to font:

    cfg://* -> ../res_mods/configs/xvm/*
    res://* -> ../res_mods/mods/shared_resources/*
    xvm://* -> ../res_mods/mods/shared_resources/xvm/*
    """

    font_path = __resolve_path(path).lower()
    if not vfs.file_exists(font_path):
        logging.getLogger('XFW/Fonts').error("__get_file: Font does not exists %s" % font_path)
        return None

    #do not copy RealFS fonts
    if os.path.isfile(font_path):
        return font_path

    realfs_path = loader.XFWLOADER_TEMPDIR + '/' + package_id + '/fonts/%s' % os.path.basename(font_path)
    if vfs.file_copy(font_path, realfs_path):
        return realfs_path
    else:
        logging.getLogger('XFW/Fonts').error("__get_file: Failed to copy font to RealFS: %s --> %s" % (font_path, realfs_path))
        return None



#
# API
#


def register_font(font_path, private = True, non_enumerable = False):
    """
    Register font to system or game.

    font_path      -- path to font relative to TODO
    private        -- false to install font global to system
    non_enumerable -- true to hide font for EnumFonts* WinAPI functions
    """

    logging.getLogger('XFW/Fonts').info("register_font: register %s" % font_path)

    if __native is None:
        return

    font_path = __get_file(font_path)
    if font_path is None:
        logging.getLogger('XFW/Fonts').error("register_font: Failed to register font: empty path")
        return

    try:
        if not __native.register_font(unicode(font_path), private, non_enumerable):
            logging.getLogger('XFW/Fonts').error("register_font: Failed to register font %s" % font_path)
    except Exception:
        logging.getLogger('XFW/Fonts').exception("register_font")


def add_alias(alias, font_name):
    try:
        return __native.add_alias(alias, font_name)
    except Exception:
        logging.getLogger('XFW/Fonts').exception('add_alias')


def remove_alias(alias):
    try:
        return __native.remove_alias(alias)
    except Exception:
        logging.getLogger('XFW/Fonts').exception('remove_alias')


def unregister_font(font_path, private = True, non_enumerable = False):
    """
    Unregister font from system or game.

    font_path      -- path to font relative to TODO
    private        -- false to install font global to system
    non_enumerable -- true to hide font for EnumFonts* WinAPI functions
    """

    font_path = __get_file(font_path)
    if font_path is None:
        return

    try:
        if not __native.unregister_font(unicode(font_path), private, non_enumerable):
            logging.getLogger('XFW/Fonts').error("unregister_font: Failed to unregister font %s" % font_path)
    except Exception:
        logging.getLogger('XFW/Fonts').exception("unregister_font")



#
# XFW Loader
#

def xfw_is_module_loaded():
    global __xfw_fonts_initialized
    return __xfw_fonts_initialized


def xfw_module_init():
    global __native
    global __xfw_fonts_initialized

    if __native is None:
        __native = XFWNativeModuleWrapper(package_id, 'xfw_fonts.pyd', 'XFW_Fonts')
        __native.init_hooks()

        dir_fonts = loader.get_mod_directory_path(package_id) + '/fonts/'
        if loader.is_mod_in_realfs(package_id):
            for font in os.listdir(dir_fonts):
                register_font(font)
        else:
            for font in vfs.directory_list_files(dir_fonts, True):
                register_font(font)

        add_alias(u"dynamic" , u"DynamicDefault")
        add_alias(u"dynamic2", u"DynamicOutline")
        add_alias(u"vtype"   , u"VehicleType")
        add_alias(u"xvm"     , u"XVMSymbol")
        add_alias(u"mono"    , u"ZurichCondMono")

        __xfw_fonts_initialized = True


def xfw_module_fini():
    global __native
    if __native is not None:
        __native.deinit_hooks()
        __native = None
