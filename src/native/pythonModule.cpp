// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Imports
//

// stdlib
#include <algorithm>
#include <map>

// Windows
#include <Windows.h>

// python
#include <pybind11/pybind11.h>

// xfw.native.hooks
#include <xfw_native_hooks.h>

// xfw.fonts
#include "ttfInfo.h"



//
// Typedefs
//

typedef HFONT(__stdcall* CreateFontW_typedef) (
   int     nHeight,
   int     nWidth,
   int     nEscapement,
   int     nOrientation,
   int     fnWeight,
   DWORD   fdwItalic,
   DWORD   fdwUnderline,
   DWORD   fdwStrikeOut,
   DWORD   fdwCharSet,
   DWORD   fdwOutputPrecision,
   DWORD   fdwClipPrecision,
   DWORD   fdwQuality,
   DWORD   fdwPitchAndFamily,
   LPCWSTR lpszFace
);

typedef int(__stdcall *EnumFontFamiliesExW_typedef) (
    HDC           hdc,
    LPLOGFONTW    lpLogfont,
    FONTENUMPROCW lpEnumFontFamExProc,
    LPARAM        lParam,
    DWORD         dwFlags);



//
// Globals
//

std::map<std::wstring, std::wstring> fontMap;
CreateFontW_typedef CreateFontW_trampoline;
EnumFontFamiliesExW_typedef EnumFontFamiliesExW_trampoline;



//
// Hooks
//

HFONT __stdcall CreateFontW_Detour(
   int     nHeight,
   int     nWidth,
   int     nEscapement,
   int     nOrientation,
   int     fnWeight,
   DWORD   fdwItalic,
   DWORD   fdwUnderline,
   DWORD   fdwStrikeOut,
   DWORD   fdwCharSet,
   DWORD   fdwOutputPrecision,
   DWORD   fdwClipPrecision,
   DWORD   fdwQuality,
   DWORD   fdwPitchAndFamily,
   LPCWSTR lpszFace
){
     const wchar_t* face = lpszFace;

    std::wstring fontName(lpszFace);
    std::transform(fontName.begin(), fontName.end(), fontName.begin(), towlower);
    if (fontMap.find(fontName) != fontMap.end())
    {
        face = fontMap[fontName].c_str();
    }

    HFONT result =  CreateFontW_trampoline(
        nHeight,
        nWidth,
        nEscapement,
        nOrientation,
        fnWeight,
        fdwItalic,
        fdwUnderline,
        fdwStrikeOut,
        fdwCharSet,
        fdwOutputPrecision,
        fdwClipPrecision,
        fdwQuality,
        fdwPitchAndFamily,
        face
    );

    return result;
}

int __stdcall EnumFontFamiliesExW_Detour(
    HDC           hdc,
    LPLOGFONTW    lpLogfont,
    FONTENUMPROCW lpEnumFontFamExProc,
    LPARAM        lParam,
    DWORD         dwFlags)
{
    std::wstring fontName(lpLogfont->lfFaceName);
    std::transform(fontName.begin(), fontName.end(), fontName.begin(), towlower);
    if (fontMap.find(fontName) != fontMap.end())
    {
        auto replacement = fontMap[fontName];
        wcsncpy(lpLogfont->lfFaceName,replacement.c_str(), LF_FACESIZE);
    }


    int result =  EnumFontFamiliesExW_trampoline(hdc,lpLogfont, lpEnumFontFamExProc, lParam, dwFlags);
    return result;
}



//
// Python
//

bool Py_InitHooks()
{
    if (!XFW::Native::Hooks::HookCreate(&CreateFontW, &CreateFontW_Detour, reinterpret_cast<void**>(&CreateFontW_trampoline)))
        return false;

    if (!XFW::Native::Hooks::HookEnable(&CreateFontW))
        return false;

    if (!XFW::Native::Hooks::HookCreate(&EnumFontFamiliesExW, &EnumFontFamiliesExW_Detour, reinterpret_cast<void**>(&EnumFontFamiliesExW_trampoline)))
        return false;

    if (!XFW::Native::Hooks::HookEnable(&EnumFontFamiliesExW))
        return false;

    return true;
}


bool Py_DeinitHooks()
{
    XFW::Native::Hooks::HookDisable(&CreateFontW);
    XFW::Native::Hooks::HookDisable(&EnumFontFamiliesExW);
    return true;
}


std::string Py_RegisterFont(const std::wstring& font_path, bool isPrivate, bool not_enumerable)
{
    DWORD flags = 0;

    if (isPrivate)
        flags |= FR_PRIVATE;

    if (not_enumerable)
        flags |= FR_NOT_ENUM;

    int fontsAdded = AddFontResourceExW(font_path.c_str(), flags, 0);
    if (fontsAdded > 0)
    {
        return GetFontFamilyFromFile(font_path);
    }

    return {};
}


bool Py_UnregisterFont(const std::wstring& font_path, bool isPrivate, bool not_enumerable)
{

    DWORD flags = 0;

    if (isPrivate)
        flags |= FR_PRIVATE;

    if (not_enumerable)
        flags |= FR_NOT_ENUM;

    int fontsAdded = RemoveFontResourceExW(font_path.c_str(), flags, 0);
    if (fontsAdded > 0)
        return true;

    return false;
}


bool Py_AddAlias(const std::wstring& alias, const std::wstring& realfont)
{
    std::wstring fontName(alias);
    std::transform(fontName.begin(), fontName.end(), fontName.begin(), towlower);
    fontMap[fontName] = realfont;
    return true;
}


bool Py_RemoveAlias(const std::wstring& alias)
{
    std::wstring fontName(alias);
    std::transform(fontName.begin(), fontName.end(), fontName.begin(), towlower);
    return fontMap.erase(fontName) > 0;
}



//
// Module
//

PYBIND11_MODULE(XFW_Fonts, m) {
    m.doc() = "XFW Fonts module";
    
    m.def("init_hooks", &Py_InitHooks);
    m.def("deinit_hooks", &Py_DeinitHooks);
    
    m.def("register_font", &Py_RegisterFont);
    m.def("unregister_font", &Py_UnregisterFont);

    m.def("add_alias", &Py_AddAlias);
    m.def("remove_alias", &Py_RemoveAlias);
}
