# Changelog

## v11.0.0

* native: MT 1.30.0 support

## v10.0.2

* python: switch to OpenWG VFS

## v10.0.1

* python: fix hooks init/deinit

## v10.0.0

* First release as separate package